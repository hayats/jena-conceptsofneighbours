package connor;

import connor.matchTree.LazyJoinResults;
import connor.matchTree.MatchTree;
import connor.utils.*;
import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.syntax.Element;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class used for the representation and computation of concepts of neighbours for a given graph and a given target
 *
 * @author hayats
 * @author nk-fouque
 */
public class ConnorPartition implements Jsonable {
	private final ConnorModel model;
	private final List<String> target;
	private final PriorityQueue<ConceptOfNeighbours> preConcepts;
	private final List<ConceptOfNeighbours> concepts;

	private int nextClusterId;

	private final int maxDescriptionDepth;

	private final Map<Node, Var> varsByUri = new HashMap<>(); // map uris and literals to vars
	private final Map<Node, Integer> uriDepth = new HashMap<>();
	private int nextKey = 0; // for var numbering
	private int partitioningSteps = 0;

	/**
	 * Standard constructor to create a partition before computation of the concepts of neighbors
	 *
	 * @param model      the model connected to the partition
	 * @param targetUris the uris of the target of the concepts of neighbours
	 * @param initTable  the set of examples to partition
	 */
	public ConnorPartition(ConnorModel model, List<String> targetUris, Table initTable, int maxDepth) {
		this.target = targetUris;
		this.nextClusterId = 0;
		this.model = model;

		this.maxDescriptionDepth = maxDepth;

		int i = 0;
		List<Var> projectionVars = new ArrayList<>();
		for(String uri : this.target) {
			Var var = Var.alloc("Neighbor_" + i);
			projectionVars.add(var);
			this.addVar(ResourceFactory.createResource(uri).asNode(), var);
			i++;
		}

		this.target.forEach(uri -> this.setDepth(ResourceFactory.createResource(uri).asNode(), 0));

		Set<Element> initialDescription = ElementUtils.getInitialDescription(this.target, this);

		this.preConcepts = new PriorityQueue<>();
		this.preConcepts.add(new ConceptOfNeighbours(initialDescription, projectionVars, this.getNextClusterId(), initTable));

		this.concepts = new ArrayList<>();

	}

	/**
	 * Used to set the depth of a node, i.e the distance from the target node to this node. Shouldn't be used by the user.
	 */
	public void setDepth(Node node, int depth) {
		if(!this.uriDepth.containsKey(node) || this.uriDepth.get(node) > depth) {
			this.uriDepth.put(node, depth);
		}
	}

	/**
	 * Used to get the depth of a node, i.e the distance from the target nodes to this node.
	 */
	public int getDepth(Node node) {
		return this.uriDepth.get(node);
	}

	public ConnorModel getModel() {
		return this.model;
	}

	private int getNextClusterId() {
		this.nextClusterId++;
		return nextClusterId;
	}

	/**
	 * Add an entry to the precomputed answers. Should not be used by the user.
	 *
	 * @param element The element for which the answer has been computed
	 * @param res     The computed answer
	 */
	public void addAns(Element element, Table res) {
		this.model.addAns(element, res);
	}

	private void addVar(Node node, Var var) {
		this.varsByUri.put(node, var);
	}

	/**
	 * Return a var for a given uri, either a new one or a pre-existing one
	 *
	 * @param node the uri corresponding to the var to create
	 * @return The var corresponding to the uri
	 */
	public Var getOrCreateVar(Node node) {
		if(this.varsByUri.containsKey(node)) {
			return this.varsByUri.get(node);
		} else {
			Var var = Var.alloc("x" + this.nextKey);
			this.nextKey++;
			this.varsByUri.put(node, var);
			return var;
		}
	}

	/**
	 * Return the set of possible answers to the query composed of only one element, if it has already been computed
	 *
	 * @param elt The element of the query
	 * @return The table representing the answers
	 */
	public Table getAnswer(Element elt) throws TableException {
		return this.model.getAnswer(elt);
	}

	/**
	 * Execute one step of the partitioning algorithm.
	 *
	 * @return true if the partitioning is over, false otherwise
	 * @throws PartitionException if we try to resume a finished partitioning
	 * @throws TableException     if there are errors during table operations
	 */
	private boolean oneStepPartitioning() throws PartitionException, TableException {
		if(this.preConcepts.isEmpty()) {
			throw new PartitionException("Tried to resume finished partition");
		}

		this.partitioningSteps++;

		// Selection of the pre-concept to refine
		ConceptOfNeighbours preConcept = this.preConcepts.poll(); // The concept to refine

		if(preConcept.getAvailableElements().isEmpty()) { // if the concept is already totally refined
			this.preConcepts.remove(preConcept);
			this.concepts.add(preConcept);
			return this.preConcepts.isEmpty();
		}

		// Selection of the element used for the refinement
		Iterator<Element> iter = preConcept.getAvailableElements().iterator();
		Set<Var> mentionedVars;
		Element elt;
		do {
			elt = iter.next();
			mentionedVars = ElementUtils.mentioned(elt);
		} while(iter.hasNext() && !preConcept.connected(mentionedVars));
		if(!preConcept.connected(mentionedVars)) {
			this.preConcepts.remove(preConcept);
			this.concepts.add(preConcept);
			return this.preConcepts.isEmpty();
		}

		// Intensions of the new (pre-)concepts
		Set<Element> intensionWithoutElt = preConcept.getIntensionBody();
		Set<Element> intensionWithElt = new HashSet<>(intensionWithoutElt);
		intensionWithElt.add(elt);

		// Match-trees of the new (pre-)concepts
		MatchTree matchTreeWithoutElt = preConcept.getMatchTree();
		MatchTree matchTreeWithElt = new MatchTree(matchTreeWithoutElt);
		MatchTree newMatchTree = new MatchTree(elt, this, preConcept.getConnectedVars());
		LazyJoinResults result = matchTreeWithElt.lazyJoin(newMatchTree);
		matchTreeWithElt = result.getMatchTree();

		// Proper extensions of the new (pre-)concepts
		Table properExtWithElt;
		Table properExtWithoutElt;
		properExtWithElt = preConcept.getProperExtension().intersect(matchTreeWithElt.getMatchSet().projection(preConcept.getProjectionVars()));
		properExtWithoutElt = preConcept.getProperExtension().difference(properExtWithElt);

		this.preConcepts.remove(preConcept);

		if(!properExtWithElt.isEmpty()) {
			// Creation of the concept including the element
			Set<Element> availableEltsWithElt = new HashSet<>(preConcept.getAvailableElements());
			availableEltsWithElt.remove(elt);

			Set<Element> removedEltsWithElt = new HashSet<>(preConcept.getRemovedElements());
			removedEltsWithElt.add(elt);

			Set<Var> connectedVarsWithElt = new HashSet<>(preConcept.getConnectedVars());
			connectedVarsWithElt.addAll(ElementUtils.mentioned(elt));

			int extDistWithElt = matchTreeWithElt.getMatchSet().projection(preConcept.getProjectionVars()).size();

			ConceptOfNeighbours preConceptWithElt = new ConceptOfNeighbours(
					this.getNextClusterId(),
					preConcept.getProjectionVars(),
					intensionWithElt,
					properExtWithElt,
					availableEltsWithElt,
					removedEltsWithElt,
					connectedVarsWithElt,
					matchTreeWithElt,
					extDistWithElt,
					preConcept.getRelaxationDistance()
			);

			if(availableEltsWithElt.isEmpty()) {
				this.concepts.add(preConceptWithElt);
			} else {
				this.preConcepts.add(preConceptWithElt);
			}
		}

		if(!properExtWithoutElt.isEmpty()) {
			// Creation of the concept excluding the element
			Set<Element> removedEltsWithoutElt = preConcept.getRemovedElements();
			removedEltsWithoutElt.add(elt);

			Set<Element> availableEltsWithoutElt = preConcept.getAvailableElements();
			availableEltsWithoutElt.remove(elt);
			Set<Element> relaxation = ElementUtils.relax(elt, this);
			relaxation.removeAll(removedEltsWithoutElt);
			availableEltsWithoutElt.addAll(relaxation);

			Set<Var> connectedVarsWithoutElt = preConcept.getConnectedVars();

			int extDistWithoutElt = matchTreeWithoutElt.getMatchSet().projection(preConcept.getProjectionVars()).size();

			ConceptOfNeighbours preConceptWithElt = new ConceptOfNeighbours(
					this.getNextClusterId(),
					preConcept.getProjectionVars(),
					intensionWithoutElt,
					properExtWithoutElt,
					availableEltsWithoutElt,
					removedEltsWithoutElt,
					connectedVarsWithoutElt,
					matchTreeWithoutElt,
					extDistWithoutElt,
					preConcept.getRelaxationDistance() + 1
			);

			if(availableEltsWithoutElt.isEmpty()) {
				this.concepts.add(preConceptWithElt);
			} else {
				this.preConcepts.add(preConceptWithElt);
			}
		}

		return this.preConcepts.isEmpty();
	}

	/**
	 * Run the partitioning algorithm, interruptible thanks to an AtomicBoolean.
	 *
	 * @param cut The atomic boolean used to interrupt the partitioning. When switched to true, the algorithm will be interrupted after the current step.
	 */
	public void fullPartitioning(AtomicBoolean cut) throws PartitionException, TableException {
		boolean isOver = false;
		while(!cut.get() && !isOver) {
			isOver = this.oneStepPartitioning();
		}
	}

	/**
	 * Used to saturate the intensions of the concepts (in type only as for today)
	 */
	public void saturateIntensions() {
		for(ConceptOfNeighbours concept : this.concepts) {
			concept.saturateIntension(this.model);
		}

		for(ConceptOfNeighbours concept : this.preConcepts) {
			concept.saturateIntension(this.model);
		}
	}

	/**
	 * @return the value set as the max description depth, i.e the max depth of the intensions of the concepts of the partition.
	 */
	public int getMaxDescriptionDepth() {
		return maxDescriptionDepth;
	}

	/**
	 * @return Serialize the partition as a JSON object for further processing
	 */
	@Override
	public String toJson() {
		StringBuilder res = new StringBuilder();
		StringBuilder uris = new StringBuilder();
		for(String uri : this.target) {
			if(uris.toString().equals("")) {
				uris.append("[");
			} else {
				uris.append(",");
			}
			uris.append("\"").append(uri).append("\"");
		}
		uris.append("]");
		res.append("{\n\"target\":").append(uris).append(",\n");
		res.append("\"fully_processed\":").append(this.preConcepts.isEmpty()).append(",\n");
		res.append("\"conceptsOfNeighbours\":[\n\t");
		List<ConceptOfNeighbours> concepts = new ArrayList<>();
		concepts.addAll(this.preConcepts);
		concepts.addAll(this.concepts);
		concepts.sort(Comparator.comparingInt(ConceptOfNeighbours::getExtensionalDistance));
		StringBuilder conceptsString = new StringBuilder();

		concepts.forEach(c -> {
			if(!conceptsString.toString().equals("")) {
				conceptsString.append(",");
			}
			conceptsString.append("\t").append(c.toJson().replaceAll("\n", "\n\t").replaceAll("\"\",", ""));
		});
		res.append(conceptsString);
		res.append("]\n}");
		return res.toString();
	}

	/**
	 * @return the number of fully refined concepts
	 */
	public int getNbConcepts() {
		return this.concepts.size();
	}

	/**
	 * @return the number of partially refined concepts
	 */
	public int getNbPreConcepts() {
		return this.preConcepts.size();
	}

	/**
	 * @return the number of refining step made during the computation of concepts
	 */
	public int getPartitioningSteps() {
		return partitioningSteps;
	}
}
